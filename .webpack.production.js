require('dotenv').config({ path: './.env.production' });

// -----------------------------------------------------------------------------

const path = require('path');
const webpack = require('webpack');

const HtmlWebPackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

// -----------------------------------------------------------------------------

module.exports = {
	devtool: false,

	entry: './src/index.tsx',

	mode: 'production',

	module: {
		rules: [
			{
				test: /.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},

			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				loaders: [
					'babel-loader',
					{
						loader: 'ts-loader',
					},
				],
			},

			{
				test: /\.scss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							modules: {
								localIdentName: '[hash:base64:5]',
							},
							sourceMap: false,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: false,
						},
					},
					{
						loader: 'sass-loader',
						options: {
							// prependData: '@import "App/include.scss";',
							sassOptions: {
								includePaths: [__dirname, 'src'],
							},
							sourceMap: false,
						},
					},
				],
			},

			{
				test: /.html$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'html-loader',
					},
				],
			},
		],
	},

	optimization: {
		minimize: true,
		minimizer: [
			new TerserPlugin({
				extractComments: false,
				sourceMap: false,
				terserOptions: {
					compress: {
						defaults: true,
						dead_code: true,
					},
				},
			}),
		],
	},

	output: {
		chunkFilename: '[name].[chunkhash].js',
		filename: '[name].[chunkhash].js',
		globalObject: 'window',
		path: path.resolve(__dirname, 'dist'),
	},

	plugins: [
		new webpack.DefinePlugin({
			ENVIRONMENT: JSON.stringify(process.env.ENVIRONMENT),
			RESUME_HOST: JSON.stringify(process.env.RESUME_HOST),
		}),
		new HtmlWebPackPlugin({
			template: './src/index.html',
			filename: './index.html',
		}),
		new ScriptExtHtmlWebpackPlugin({
			defaultAttribute: 'defer',
		}),
		new ProgressBarPlugin({
			width: 80,
			format: '[:bar] :percent (:elapseds)',
		}),
		new MiniCssExtractPlugin({
			filename: '[name].[chunkhash].css',
			chunkFilename: '[name].[chunkhash].css',
		}),
		// new BundleAnalyzerPlugin(),
	],

	resolve: {
		extensions: ['.js', '.ts', '.tsx'],
		modules: [path.resolve('./src'), path.resolve('./node_modules')],
	},

	stats: {
		children: false,
		entrypoints: false,
	},
};
