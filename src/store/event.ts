// graphql is cool, but I often miss the simplicity of the early version of
// FLUX. But I guess facebook can't leave well enough alone.

import { StoreonStore } from 'storeon';
import { route } from 'preact-router';

import fetchThis from 'lib/fetch';
import { orArr } from 'lib/or';
import intoResumeIdObject from 'lib/into_resume_id_object';

import Store from 'store';

// -----------------------------------------------------------------------------

const IS_ARRAY = {
	fullName: false,
	byline: false,
	contactPhoneNumber: false,
	contactEmail: false,
	personalStatement: false,
	expertise: true,
	reference: true,
	experience: true,
};

const ORDER = [
	'fullName',
	'byline',
	'contactPhoneNumber',
	'contactEmail',
	'personalStatement',
	'expertise',
	'reference',
	'experience',
];

// -----------------------------------------------------------------------------

export default function wireUpEvents(store: StoreonStore): void {
	store.on('resume:create', resumeCreate);
	store.on('resume:creating', resumeCreating);
	store.on('resume:creatingFailed', resumeCreatingFailed);

	store.on('resume:read', resumeRead);
	store.on('resume:readComplete', resumeReadComplete);
	store.on('resume:reading', resumeReading);
	store.on('resume:readingFailed', resumeReadingFailed);

	store.on('resume:data:upsert', resumeDataUpsert);

	store.on('resume:data:delete', resumeDataDelete);
}

// -----------------------------------------------------------------------------

async function resumeCreate() {
	try {
		Store.dispatch('resume:creating');

		//
		const { hash } = await fetchThis(`v1/editor/resume/create`);

		//
		route(`/${hash}`);
	} catch (error) {
		Store.dispatch('resume:creatingFailed');
		console.log('Server Error:', error);
	}
}

function resumeCreating() {
	return { resumeCreating: true };
}

function resumeCreatingFailed() {
	return { resumeCreating: false, resumeCreatingFailed: true };
}

// -----------------------------------------------------------------------------

async function resumeRead(state: StoreonStore, hash: string) {
	try {
		Store.dispatch('resume:reading');

		//
		const resume = await fetchThis(`v1/editor/resume`, {
			hash,
		});

		//
		Store.dispatch('resume:readComplete', resume);
	} catch (error) {
		Store.dispatch('resume:readingFailed');
		console.log('Server Error:', error);
	}
}

function resumeReading() {
	return { resumeReading: true };
}

function resumeReadComplete(store: StoreonStore, resume: GenericObject) {
	return {
		resume,
		resumeReading: false,
		resumeReadingFailed: false,
	};
}

function resumeReadingFailed() {
	return { resumeReading: false, resumeReadingFailed: true };
}

// -----------------------------------------------------------------------------

function resumeDataUpsert({ resume }: GenericObject, { type, id, value }) {
	const { data = [], resume: { structure = [] } = {} } = resume;

	//
	let modifiedExisting = false;
	const newData = orArr(data).reduce(
		(arr: GenericObject[], row: GenericObject): GenericObject[] => {
			if (row.id === id && type === type) {
				modifiedExisting = true;
				arr.push({
					...row,
					updated: new Date(),
					value,
				});

				return arr;
			}

			// yes, let's create a new array every iteration and do a sugar-slice
			// that gets slower every iteration.
			return [...arr, row];
		},
		[],
	);

	//
	let newStructure = structure;
	if (!modifiedExisting) {
		newData.push({ id, type, value, create: new Date(), updated: new Date() });

		const resumeObj = orArr(structure).reduce(intoResumeIdObject, {});
		newStructure = ORDER.map((key) => {
			const isArray = IS_ARRAY[key];
			let newValue = resumeObj[key] || (isArray ? [] : null);

			if (key === type) {
				if (isArray) {
					newValue = [...newValue, id];
				} else {
					newValue = id;
				}
			}

			// wait, didn't you just say...
			// no, see, now it's different because I'm lazy
			return [key, newValue];
		});
	}

	// W-what is this? Tech debt. ...Tech debt.
	return {
		resume: {
			...resume,
			resume: {
				...resume.resume,
				structure: newStructure,
				updated: new Date(),
			},
			data: newData,
		},
	};
}

// -----------------------------------------------------------------------------

function resumeDataDelete({ resume }: GenericObject, { type, id }): GenericObject {
	const { resume: { structure = [] } = {} } = resume;

	const resumeObj = orArr(structure).reduce(intoResumeIdObject, {});
	const newStructure = ORDER.map((key) => {
		const isArray = IS_ARRAY[key];
		let newValue = resumeObj[key] || (isArray ? [] : null);

		if (key === type) {
			newValue = isArray ? newValue.filter((rid: number) => rid !== id) : null;
		}

		return [key, newValue];
	});

	//
	return {
		resume: {
			...resume,
			resume: {
				...resume.resume,
				structure: newStructure,
				updated: new Date(),
			},
		},
	};
}
