import Store from 'store';

// -----------------------------------------------------------------------------

export function createResume() {
	Store.dispatch('resume:create');
}

export function readResume(hash: string) {
	Store.dispatch('resume:read', hash);
}

export function updateResume(resume: GenericObject) {
	Store.dispatch('resume:update', resume);
}

export function upsertResumeData(type: string, id: number, value: string[] | GenericObject) {
	Store.dispatch('resume:data:upsert', { type, id, value });
}

export function deleteResumeData(type: string, id: number) {
	Store.dispatch('resume:data:delete', { type, id });
}
