// with the exception of headless imports, I like to organization my imports
// roughly alphabetical in decreasing "distance" from the code in question

import './style.scss';

import { h, render, VNode } from 'preact';
import Router from 'preact-router';
import AsyncRoute from 'preact-async-route';
import { StoreContext } from 'storeon/preact';

import Store from 'store';

// -----------------------------------------------------------------------------

export default function renderApp(): void {
	while (document.body.firstChild) {
		document.body.removeChild(document.body.firstChild);
	}

	//
	render(
		<StoreContext.Provider value={Store}>
			<App />
		</StoreContext.Provider>,
		document.body,
	);
}

// -----------------------------------------------------------------------------

function App(): VNode {
	return (
		<Router>
			<AsyncRoute path="/" getComponent={getHomePage} />
			<AsyncRoute path="/:hash" getComponent={getEditorPage} />
		</Router>
	);
}

// -----------------------------------------------------------------------------

function getHomePage() {
	return import(
		/* webpackChunkName: "co-ho" */
		'component/HomePage'
	).then(returnComponent);
}

function getEditorPage() {
	return import(
		/* webpackChunkName: "co-ed" */
		'component/EditorPage'
	).then(returnComponent);
}

function returnComponent({ default: component }) {
	return component;
}
