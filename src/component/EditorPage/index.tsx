import { h, VNode } from 'preact';
import { useEffect } from 'preact/hooks';
import { useStoreon } from 'storeon/preact';

import { readResume } from 'store/action';

import LoadingIndicator from 'component/Shared/FullScreenLoadingIndicator';

import Resume from './Resume';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function EditorPage({ hash }): VNode {
	const { resume, resumeReading, resumeReadingFailed } = useStoreon(
		'resume',
		'resumeReading',
		'resumeReadingFailed',
	);

	useEffect(() => readResume(hash), [hash]);

	//
	if (resumeReading) {
		return <LoadingIndicator />;
	}

	//
	return resumeReadingFailed ? (
		<div class={c.error}>
			There was an error. <a href="/">Go home.</a>
		</div>
	) : (
		<Resume {...resume} />
	);
}
