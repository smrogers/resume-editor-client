import { h, VNode } from 'preact';

import { bindUnd } from 'lib/bindto';

import TwoControl from 'component/EditorPage/Resume/TwoControl';

// -----------------------------------------------------------------------------

const LABELS = ['Skillset', 'Timeframe'];

// -----------------------------------------------------------------------------

export default function ResumeExpertises({ hash, data }: ResumeExpertisesProps): VNode {
	return (
		<div>
			{renderExpertises(hash, data)}
			<TwoControl type="expertise" labels={LABELS} hash={hash} isNew={true} key="expertise-new" />
		</div>
	);
}

// -----------------------------------------------------------------------------

function renderExpertises(hash: string, data: GenericObject): VNode[] {
	return data.map(bindUnd(renderExpertise, hash));
}

function renderExpertise(hash: string, { id, value = [] }): VNode {
	return (
		<TwoControl
			type="expertise"
			labels={LABELS}
			hash={hash}
			id={id}
			value={value}
			key={`expertise-${id}`}
		/>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeExpertisesProps {
	hash: string;
	data: GenericObject[];
}
