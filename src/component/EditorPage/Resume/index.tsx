import { h, VNode } from 'preact';

import { orArr } from 'lib/or';
import intoResumeIdObject from 'lib/into_resume_id_object';

import Content from 'component/Shared/Content';

import Control from './Control';
import Experiences from './Experiences';
import Expertises from './Expertises';
import References from './References';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function Resume({ resume: rawResume = {}, data: rawData = [] }): VNode {
	const data: GenericObject = orArr(rawData).reduce(intoDataObject, {});
	const { hash, slug, structure }: GenericObject = rawResume;
	const {
		fullName,
		byline,
		contactPhoneNumber,
		contactEmail,
		personalStatement,
		expertise = [],
		reference = [],
		experience = [],
	} = orArr(structure).reduce(intoResumeIdObject, {});
	const toData = (id: number) => data[id];

	return (
		<Content>
			<h2 class={c.header}>
				<a href="/">Home</a>
			</h2>
			<div class={c.url}>
				<div>Your public resume URL:</div>

				<a href={`${RESUME_HOST}/${slug}`}>
					{RESUME_HOST}/{slug}
				</a>

				<div class={c.privateURL}>Your private editor URL:</div>
				<a href={`/${hash}`}>
					{window.location.origin}/{hash}
				</a>
			</div>
			<div class={c.row}>
				<Control
					label="Full Name"
					hash={hash}
					type="fullName"
					id={fullName}
					data={data[fullName]}
				/>
			</div>
			<div class={c.row}>
				<Control label="Byline" hash={hash} type="byline" id={byline} data={data[byline]} />
			</div>
			<div class={c.row}>
				<Control
					label="Contact Phone Number"
					hash={hash}
					type="contactPhoneNumber"
					id={contactPhoneNumber}
					data={data[contactPhoneNumber]}
				/>
			</div>
			<div class={c.row}>
				<Control
					label="Contact Email"
					hash={hash}
					type="contactEmail"
					id={contactEmail}
					data={data[contactEmail]}
				/>
			</div>
			<div class={c.row}>
				<Control
					label="Personal Statement"
					hash={hash}
					type="personalStatement"
					id={personalStatement}
					data={data[personalStatement]}
					input={false}
				/>
			</div>
			<div class={c.row}>
				<strong>Expertise</strong>
				<Expertises hash={hash} data={expertise.map(toData)} />
			</div>
			<div class={c.row}>
				<strong>References</strong>
				<References hash={hash} data={reference.map(toData)} />
			</div>
			<div class={c.row}>
				<strong>Experiences</strong>
				<Experiences hash={hash} data={experience.map(toData)} />
			</div>
		</Content>
	);
}

// -----------------------------------------------------------------------------

function intoDataObject(obj: GenericObject, data: GenericObject): GenericObject {
	obj[data?.id] = data;

	return obj;
}
