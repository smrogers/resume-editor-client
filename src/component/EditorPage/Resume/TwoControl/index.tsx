import { Component, h, RefObject, VNode } from 'preact';
import { useRef } from 'preact/hooks';

import bindTo from 'lib/bindto';
import { classIf, classname } from 'lib/class';
import getJson from 'lib/get_json';

import { deleteResumeData, upsertResumeData } from 'store/action';

import LoadingIndicator from 'component/Shared/LoadingIndicator';

import c from './style.scss';

// -----------------------------------------------------------------------------

// And the reward for the most intelligently named controls goes to...
// Not this one!

export default class ResumeTwoControl extends Component {
	props: GenericObject;
	state: GenericObject;
	timerId: number;
	refs: RefObject<HTMLInputElement>[];

	// -----------------------------------------------------------------------------

	constructor(props: ResumeReferenceProps) {
		super(props);

		this.state = { ...props };
		this.refs = [useRef(), useRef()];
	}

	// -----------------------------------------------------------------------------

	render(): VNode {
		const {
			isDirty,
			isCommunicating,
			isNew,
			// quality variable names, muah
			labels: [firstLabel = '', secondLabel = ''],
			value: [firstValue = '', secondValue = ''] = [],
		} = this.state;

		return (
			<div class={classname(classIf(isDirty, c.rowDirty, c.row), classIf(isNew, c.new))}>
				<div class={c.inputs}>
					<input
						type="text"
						value={firstValue}
						onBlur={bindTo(this.onChange, this, 0)}
						placeholder={firstLabel}
						ref={this.refs[0]}
						key="input-0"
					/>
					<input
						type="text"
						value={secondValue}
						onBlur={bindTo(this.onChange, this, 1)}
						placeholder={secondLabel}
						ref={this.refs[1]}
						key="input-1"
					/>
				</div>
				{this.renderDeleteButton(isNew)}
				{this.renderLoadingIndicator(isCommunicating)}
			</div>
		);
	}

	// -----------------------------------------------------------------------------

	renderDeleteButton(isNew: boolean): VNode {
		return isNew ? null : <button onClick={bindTo(this.delete, this)}>✖</button>;
	}

	renderLoadingIndicator(isCommunicating: boolean): VNode {
		return isCommunicating ? (
			<div class={c.action}>
				<LoadingIndicator />
			</div>
		) : null;
	}

	// -----------------------------------------------------------------------------

	onChange(index: number, e: Event) {
		clearTimeout(this.timerId || -1);

		// the following might be my only comment in this
		// entire project:
		// give the browser some time to update the activeElement
		setTimeout(bindTo(this.onTick, this));

		//
		const inputValue: string = (e.target as HTMLInputElement).value;
		const { value = [] } = this.state;
		const oldValue = value[index] || '';

		if (oldValue === inputValue) {
			return;
		}

		//
		const newValue = [...value];
		newValue[index] = inputValue;

		this.setState({ isDirty: true, value: newValue });
	}

	onUpdatedData({ id: newId }: GenericObject = {}) {
		const { type } = this.props;
		const { id, isNew, value = [] } = this.state;

		//
		this.setState({
			value: isNew ? [] : value,
			isDirty: false,
			isCommunicating: false,
		});

		//
		upsertResumeData(type, newId || id, value);
	}

	onDeleted() {
		this.setState({ isDirty: false, isCommunicating: false });

		//
		const { id, type } = this.props;
		deleteResumeData(type, id);
	}

	onTick() {
		if (this.hasActiveElement()) {
			return;
		}

		this.timerId = (setTimeout(bindTo(this.updateData, this), 1000) as unknown) as number;
	}

	// -----------------------------------------------------------------------------

	updateData() {
		clearTimeout(this.timerId);

		//
		const { isNew, type, value: originalValue = [] } = this.props;
		const { hash, id, value = [] } = this.state;

		if (isEqual(originalValue, value) || !isValid(value)) {
			return this.setState({ isDirty: false, isCommunicating: false });
		}

		//
		this.setState({ isDirty: true, isCommunicating: true });

		//
		const action: string = isNew ? 'create' : 'update';

		fetch(`/api/v1/editor/resume/data/${action}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				hash,
				type,
				id,
				value,
			}),
		})
			.then(getJson)
			.then(bindTo(this.onUpdatedData, this));
		//			.catch(this.onError.bind(this));
	}

	delete() {
		this.setState({ isDirty: true, isCommunicating: true });

		//
		const { hash, id } = this.state;

		fetch(`/api/v1/editor/resume/data/delete`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				hash,
				id,
			}),
		})
			.then(getJson)
			.then(bindTo(this.onDeleted, this));
		//			.catch(this.onError.bind(this));
	}

	// -----------------------------------------------------------------------------

	hasActiveElement() {
		return this.refs.some(isActiveElement);
	}
}

// -----------------------------------------------------------------------------

function isActiveElement(ref: RefObject<HTMLElement>) {
	return ref.current === document.activeElement;
}

function isValid(arr: string[]): boolean {
	// this is the equivalent of that guy that doesn't stop talking at the party.
	// Just take a sip of your drink, man, the ice is melting
	return arr.map(toTrimmed).join('').length > 0;
}

function toTrimmed(str: string = ''): string {
	return str.trim();
}

// replacement for array-version of _.isEqual
function isEqual(a: string[], b: string[]): boolean {
	return a.join('') === b.join('');
}

// typing
// -----------------------------------------------------------------------------

interface ResumeReferenceProps {
	hash: string;
	id?: number;
	value?: string[];
}
