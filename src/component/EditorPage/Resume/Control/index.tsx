// I love a functional react component, but sometimes a class just works
// so much better. And this is one of those times.
//

import { Component, createRef, h, RefObject, VNode } from 'preact';

import bindTo from 'lib/bindto';
import { classIf } from 'lib/class';
import getJson from 'lib/get_json';
import isValidInput from 'lib/is_valid_input';

import { upsertResumeData } from 'store/action';

import LoadingIndicator from 'component/Shared/LoadingIndicator';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default class ResumeControl extends Component {
	props: GenericObject;
	state: GenericObject;

	timerId: number;
	ref: RefObject<HTMLElement>;

	// -----------------------------------------------------------------------------

	constructor(props) {
		super(props);

		this.state = { ...(props.data || {}) };
		this.ref = createRef();
	}

	render(): VNode {
		const { input = true, label } = this.props;
		const { value, isDirty, isCommunicating } = this.state;

		return (
			<div class={classIf(isDirty, c.controlDirty, c.control)}>
				<strong>
					{label}
					{this.renderLoadingIndicator(isCommunicating)}
				</strong>
				{this.renderControl(value, input)}
			</div>
		);
	}

	// -----------------------------------------------------------------------------

	renderControl(value: string, isInput: boolean): VNode {
		const onChange = bindTo(this.onChange, this);

		return isInput ? (
			<input type="text" value={value} onChange={onChange} />
		) : (
			<textarea onChange={onChange}>{value}</textarea>
		);
	}

	renderLoadingIndicator(isCommunicating: boolean): VNode {
		return isCommunicating ? (
			<div class={c.action}>
				<LoadingIndicator />
			</div>
		) : null;
	}

	// -----------------------------------------------------------------------------

	onChange(e: Event) {
		clearTimeout(this.timerId || -1);

		//
		const { data: { value: oldValue = '' } = {} } = this.props;
		const newValue: string = (e.target as HTMLInputElement).value;
		const isDirty = newValue !== oldValue;

		this.setState({ isDirty, value: newValue });

		//
		if (isDirty || !newValue) {
			this.timerId = (setTimeout(bindTo(this.updateData, this), 1000) as unknown) as number;
		}
	}

	onUpdatedData() {
		this.setState({ isDirty: false, isCommunicating: false });

		const { type, id, value } = this.state;
		upsertResumeData(type, id, value);

		// Something this component doesn't support--nor any other at the time of
		// this comment's writing--is network error handling. Why? That's a good
		// question. <-- This is an orphan. Well, not anymore.
	}

	// -----------------------------------------------------------------------------

	updateData() {
		clearTimeout(this.timerId);

		//
		const { hash, type, data: { value: oldValue = undefined } = {} } = this.props;
		const { id, value } = this.state;

		// Something that would be added in a future version of my Resume startup,
		// Resulvie, valued at 1.23 billion at the second angel investment round
		// after the world's first pre-pre-alpha-- okay, that went on long enough.

		// Anyway, the control doesn't has an invalid input state.

		if (value === oldValue || !isValidInput(value)) {
			return this.setState({ isDirty: false, isCommunicating: false });
		}

		//
		// this isn't code duplication because this never happens
		// if the above returns.
		this.setState({ isDirty: true, isCommunicating: true });

		//
		const action: string = id === undefined ? 'create' : 'update';

		fetch(`/api/v1/editor/resume/data/${action}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				hash,
				type,
				id,
				value,
			}),
		})
			.then(getJson)
			.then(bindTo(this.onUpdatedData, this));
		//			.catch(this.onError.bind(this));
	}
}
