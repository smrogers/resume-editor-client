// I could merge Expertises and References together since they are the same
// code and only difer in the labels, but I feel the two component's content
// is different enough--even if identical to the database--to warrant logical
// separation. KISS can include copy pasting code and changing a few strings.

// "But then why didn't you leave the Expertise and Reference components
// separate?"
//
// Standup answer: The content within the TwoControl component can be separated
// from the higher component pages so it can be safely extracted without causing
// problems later on.
//
// Real answer: It was late and I stopped caring. If I were going to continue
// building an actual resume site, I might revisit in a refactor. But 80/20,
// "don't let perfect be the enemy of the good", buzz-yadda, word-yadda

// -----------------------------------------------------------------------------

import { h, VNode } from 'preact';

import { bindUnd } from 'lib/bindto';

import TwoControl from 'component/EditorPage/Resume/TwoControl';

// -----------------------------------------------------------------------------

const LABELS = ['Person', 'Contact'];

// -----------------------------------------------------------------------------

export default function ResumeReferences({ hash, data }: ResumeReferencesProps): VNode {
	// I'm not going to implement list sorting because 1) I'm trying to limit the
	// big ticket modules so 2) I ain't dealing with the complicated drag/drop BS
	// unless I'm getting paid to do so.

	// "Yeah, but you could just add two buttons that--He ran away. How did he
	// do that in text? W-wait, who is writing this...?"

	return (
		<div>
			{renderReferences(hash, data)}
			<TwoControl type="reference" labels={LABELS} isNew={true} hash={hash} key="reference-new" />
		</div>
	);
}

// -----------------------------------------------------------------------------

function renderReferences(hash: string, data: GenericObject): VNode[] {
	return data.map(bindUnd(renderReference, hash));
}

function renderReference(hash: string, { id, value = [] }): VNode {
	return (
		<TwoControl
			type="reference"
			labels={LABELS}
			hash={hash}
			id={id}
			value={value}
			key={`reference-${id}`}
		/>
	);
}

// typing
// -----------------------------------------------------------------------------

interface ResumeReferencesProps {
	hash: string;
	data: GenericObject[];
}
