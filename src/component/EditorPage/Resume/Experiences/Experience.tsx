// This could be merged with TwoControl into a Parent component that
// uses a child component as the form, but that's a whole level of
// hassle for passing around callbacks I want to avoid.

// Simple is best.

import { Component, h, RefObject, VNode } from 'preact';
import { useRef } from 'preact/hooks';

import bindTo from 'lib/bindto';
import { classIf, classname } from 'lib/class';
import getJson from 'lib/get_json';

import { deleteResumeData, upsertResumeData } from 'store/action';

import LoadingIndicator from 'component/Shared/LoadingIndicator';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default class ResumeExperience extends Component {
	props: GenericObject;
	state: GenericObject;
	timerId: number;
	refs: RefObject<any>[];

	// -----------------------------------------------------------------------------

	constructor(props: ResumeReferenceProps) {
		super(props);

		this.state = { ...props };
		this.refs = [useRef(), useRef(), useRef(), useRef()];
	}

	// -----------------------------------------------------------------------------

	render(): VNode {
		const {
			isDirty,
			isCommunicating,
			isNew,
			value: { areas = '', description = '', timeframe = '', title = '' } = {},
		} = this.state;

		return (
			<div class={classname(classIf(isDirty, c.rowDirty, c.row), classIf(isNew, c.new))}>
				<section class={c.inputs}>
					<div>
						<input
							type="text"
							value={title}
							placeholder="Company/label"
							onBlur={bindTo(this.onChange, this, 'title')}
							ref={this.refs[0]}
						/>
						<input
							type="text"
							value={timeframe}
							placeholder="Timeframe"
							onBlur={bindTo(this.onChange, this, 'timeframe')}
							ref={this.refs[1]}
						/>
					</div>
					<div>
						<input
							type="text"
							value={areas}
							placeholder="Related areas"
							onBlur={bindTo(this.onChange, this, 'areas')}
							ref={this.refs[2]}
						/>
					</div>
					<div>
						<textarea
							value={description}
							placeholder="Description"
							onBlur={bindTo(this.onChange, this, 'description')}
							ref={this.refs[3]}
						/>
					</div>
				</section>
				<section class={c.actions}>
					{this.renderDeleteButton(isNew)}
					{this.renderLoadingIndicator(isCommunicating)}
				</section>
			</div>
		);
	}

	// -----------------------------------------------------------------------------

	renderDeleteButton(isNew: boolean): VNode {
		return isNew ? null : <button onClick={bindTo(this.delete, this)}>✖</button>;
	}

	renderLoadingIndicator(isCommunicating: boolean): VNode {
		return isCommunicating ? (
			<div class={c.action}>
				<LoadingIndicator />
			</div>
		) : null;
	}

	// -----------------------------------------------------------------------------

	onChange(key: string, e: Event) {
		clearTimeout(this.timerId || -1);

		// give the browser some time to update the activeElement
		setTimeout(bindTo(this.onTick, this));

		//
		const inputValue: string = (e.target as HTMLInputElement).value;
		const { value = [] } = this.state;
		const oldValue = value[key] || '';

		if (oldValue === inputValue) {
			return;
		}

		//
		const newValue = { ...value, [key]: inputValue };

		this.setState({ isDirty: true, value: newValue });
	}

	onUpdatedData({ id: newId }: GenericObject = {}) {
		const { id, isNew, value = {} } = this.state;

		//
		this.setState({
			value: isNew ? {} : value,
			isDirty: false,
			isCommunicating: false,
		});

		//
		upsertResumeData('experience', newId || id, value);
	}

	onDeleted() {
		this.setState({ isDirty: false, isCommunicating: false });

		//
		const { id } = this.props;
		deleteResumeData('experience', id);
	}

	onTick() {
		if (this.hasActiveElement()) {
			return;
		}

		this.timerId = (setTimeout(bindTo(this.updateData, this), 1000) as unknown) as number;
	}

	// -----------------------------------------------------------------------------

	updateData() {
		clearTimeout(this.timerId);

		//
		const { isNew, value: originalValue = [] } = this.props;
		const { hash, id, value = {} } = this.state;

		if (isEqual(originalValue, value) || !isValid(value)) {
			return this.setState({ isDirty: false, isCommunicating: false });
		}

		//
		this.setState({ isDirty: true, isCommunicating: true });

		//
		const action: string = isNew ? 'create' : 'update';

		fetch(`/api/v1/editor/resume/data/${action}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				hash,
				type: 'experience',
				id,
				value,
			}),
		})
			.then(getJson)
			.then(bindTo(this.onUpdatedData, this));
		//			.catch(this.onError.bind(this));
	}

	delete() {
		this.setState({ isDirty: true, isCommunicating: true });

		//
		const { hash, id } = this.state;

		fetch(`/api/v1/editor/resume/data/delete`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				hash,
				id,
			}),
		})
			.then(getJson)
			.then(bindTo(this.onDeleted, this));
		//			.catch(this.onError.bind(this));
	}

	// -----------------------------------------------------------------------------

	hasActiveElement() {
		return this.refs.some(isActiveElement);
	}
}

// -----------------------------------------------------------------------------

function isActiveElement(ref: RefObject<HTMLElement>) {
	return ref.current === document.activeElement;
}

function isValid(obj: string[]): boolean {
	// this is the equivalent of that guy that doesn't stop talking at the party.
	// Just take a sip of your drink, man, the ice is melting
	return Object.values(obj).map(toTrimmed).join('').length > 0;
}

function toTrimmed(str: string = ''): string {
	return str.trim();
}

// replacement for object-version of _.isEqual
function isEqual(a: GenericObject, b: GenericObject): boolean {
	const aKey = Object.keys(a).sort().join('');
	const bKey = Object.keys(b).sort().join('');

	const aValues = Object.values(a).join('');
	const bValues = Object.values(b).join('');

	return aKey === bKey && aValues === bValues;
}

// typing
// -----------------------------------------------------------------------------

interface ResumeReferenceProps {
	hash: string;
	id?: number;
	value?: string[];
}
