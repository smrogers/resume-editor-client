import bindTo, { bindUnd } from 'lib/bindto';
import { Fragment, h, VNode } from 'preact';

import Experience from './Experience';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function ResumeExperiences({ hash, data }: ResumeExperiencesProps): VNode {
	return (
		<div class={c.container}>
			{renderExperiences(hash, data)}
			<Experience hash={hash} isNew={true} key="experience-new" />
		</div>
	);
}

// -----------------------------------------------------------------------------

function renderExperiences(hash: string, data: GenericObject[] = []): VNode[] {
	return data.map(bindUnd(renderExperience, hash));
}

function renderExperience(hash: string, { id, value = [] }): VNode {
	return <Experience hash={hash} id={id} value={value} key={`experience-${id}`} />;
}

// typing
// -----------------------------------------------------------------------------

interface ResumeExperiencesProps {
	hash: string;
	data: GenericObject[];
}
