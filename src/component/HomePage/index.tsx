import { Fragment, h, VNode } from 'preact';
import { useStoreon } from 'storeon/preact';

import { createResume } from 'store/action';

import Content from 'component/Shared/Content';
import FullScreenLoadingIndicator from 'component/Shared/FullScreenLoadingIndicator';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function HomePage(): VNode {
	const { resumeCreating, resumeCreatingFailed } = useStoreon(
		'resumeCreating',
		'resumeCreatingFailed',
	);

	const apiLoader = resumeCreating ? <FullScreenLoadingIndicator /> : null;
	const errorMessage = resumeCreatingFailed ? (
		<div class={c.error}>There was an error creating a new resume.</div>
	) : null;
	const onClick = resumeCreating ? undefined : createResume;

	return (
		<Fragment>
			{apiLoader}
			<Content>
				<h1>Resume Editor</h1>
				<p class={c.warning}>
					This editor and the resume site is for demonstration only. I have no use for your
					information. The URLs are randomized and not publicly listed. And the database is
					regularly scrubbed. <em>Even so</em>, anything you enter into this site is accessible by
					anyone or anything with the URL. Don't enter anything you don't want me or the internet to
					know. More importantly, don't blame me when they find it if you do.
				</p>
				{errorMessage}
				<p class={c.createButton}>
					<button onClick={onClick}>Create Resume</button>
				</p>
			</Content>
		</Fragment>
	);
}
