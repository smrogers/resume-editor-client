import { h, VNode } from 'preact';

import LoadingIndicator from 'component/Shared/LoadingIndicator';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function FullScreenLoadingIndicator(): VNode {
	return (
		<div class={c.fullscreen}>
			<div class={c.container}>
				<LoadingIndicator />
			</div>
		</div>
	);
}
