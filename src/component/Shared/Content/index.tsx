import { h, VNode } from 'preact';

import { classname } from 'lib/class';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function Content({ containerClass, contentClass, children }: ContentProps): VNode {
	return (
		<div class={classname(c.container, containerClass)}>
			<div class={classname(c.content, contentClass)}>{children}</div>
		</div>
	);
}

// -----------------------------------------------------------------------------

type ContentProps = {
	containerClass?: string;
	contentClass?: string;
	children?: any;
};
