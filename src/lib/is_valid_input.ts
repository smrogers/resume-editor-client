export default function isValidInput(value: string = ''): boolean {
	return value.trim().length > 0;
}
