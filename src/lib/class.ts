export function classname(...classes: string[]): string {
	return classes.filter(identity).join(' ');
}

// this function is more about making the upstream code intent more legible
export function classIf(truthy: any, className: string, elseClassName?: string): string {
	return truthy ? className : elseClassName;
}

// -----------------------------------------------------------------------------

function identity(value: any): any {
	return value;
}
