// Throwing errors is cool

export default function throwIf(truthy: any, message: string = 'There was an error'): void {
	if (truthy) {
		throw new Error(message);
	}
}
