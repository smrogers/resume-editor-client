export default function intoResumeIdObject(obj: GenericObject, [key, ids]): GenericObject {
	obj[key] = ids;

	return obj;
}
