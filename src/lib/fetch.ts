import throwIf from 'lib/throw_if';

// -----------------------------------------------------------------------------

export default async function fetchThis(
	route: string,
	body: GenericObject = undefined,
): Promise<GenericObject> {
	const response: any = await fetch(`/api/${route}`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(body),
	});
	throwIf(response.status !== 200 && response.status !== 204);

	return (await response.json()) as GenericObject;
}
