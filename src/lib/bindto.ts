// why, yes, code can be too functional
// just like chocolate can be too good

export default function bindTo(that: any, toThis: any, ...params: any[]): any {
	return that.bind(toThis, ...params);
}

// bindUn? unbind? bindUndefined? thisWillBindTheObjectToUndefinedPlusMaybeParameters?

export function bindUnd(that: any, ...params: any[]): any {
	// Sometimes I think about how I didn't route this to the above function
	// and I wonder if I wasted the last 25 years of my life. My fiance asks
	// me what I'm thinking about as I lay beside her and I don't have the
	// heart to tell her I want to get up and run to the computer to change
	// one line of code. Like how after you noticed an uneven shoelace and
	// it just consumes your world like what kind of company would insert
	// shoelaces into their own shoes like that? Don't they have any pride
	// in their products?

	// But seriously, KISS isn't just about eliminating code duplication.
	// It's--sorry, had to retie my shoes. But... I forgot what I was
	// saying. Oh well.

	return that.bind(undefined, ...params);
}
