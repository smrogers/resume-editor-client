export function orObj(obj: GenericObject): GenericObject {
	return obj || {};
}

export function orArr<T = any[]>(arr: T[]): T[] {
	return arr || ([] as T[]);
}
