// seems dumb but memory smoothing is important to JS programs

export default function getJson(response: Response): GenericObject {
	return response.json();
}
