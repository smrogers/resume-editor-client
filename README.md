# Steven Rogers' Resume Editor Client

The resume editor client uses webpack to compile and run a Typescript-enabled preact/storeon single page app with preact-router controlling the URL state.

The build files is hosted in an S3 bucket attached to an API gateway endpoint that proxies local /api/\* requests to the API server.

## Running locally

Create an .env file:

```bash
ENVIRONMENT=development
RESUME_HOST=http://localhost:8888
API_HOST=http://localhost:8889
PORT=8890
```

Then `npm install` and `npm start`.

## Deploying to AWS

- Have local aws credentials profile called resume with generous S3 permissions
- Create an .env.production file:

```bash
ENVIRONMENT=production
RESUME_HOST=https://realhost.com
```

- `npm run deploy-production`

## Other repos

**Resume Client**: https://bitbucket.org/smrogers/resume-client

**API Server**: https://bitbucket.org/smrogers/resume-server
